package uk.vulcannetworks.chat;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class vChat implements Listener{

	private HashMap<String, String> prefixes = new HashMap<>();
	
	public void putPlayer(String player, String prefix){
		prefixes.put(player, prefix);
	}
	
	public String getPlayer(String player){
		return prefixes.get(player);
	}
	
	public void removePlayer(String player){
		prefixes.remove(player);
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent asyncPlayerChatEvent){
		asyncPlayerChatEvent.setFormat(ChatColor.translateAlternateColorCodes('&', "&8[" + "&8" + Install.getInstance().getSuperHeroManager().getPlayersHero(asyncPlayerChatEvent.getPlayer()).name() + "&8] " + getPlayer(asyncPlayerChatEvent.getPlayer().getName())) + " " + asyncPlayerChatEvent.getPlayer().getName() + " > " + ChatColor.GRAY + asyncPlayerChatEvent.getMessage());
	}
}
