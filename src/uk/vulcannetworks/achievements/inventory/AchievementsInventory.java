package uk.vulcannetworks.achievements.inventory;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;

import uk.vulcannetworks.achievements.interfaces.superclass.Achievement;
import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.hub.utils.Items;

public class AchievementsInventory implements Listener, CommandExecutor{

	private static Inventory inventory = Bukkit.createInventory(null, 45, ChatColor.GRAY + "Achievements");
	
	public void onOpen(Player p){
		inventory = Bukkit.createInventory(null, 45, ChatColor.GRAY + "Achievements");
		inventory.setItem(13, Items.getInstance.create(Material.PAPER, 1, ChatColor.GOLD + "Achievements", ChatColor.GRAY + "View your achievements below!"));
		
		int start = 27;
		
		for(Achievement achievement : Install.getInstance().getaManager().blockBreakAchievements){
			if(achievement.playerHas(p)){
				inventory.setItem(start, Items.getInstance.create(Material.ENCHANTED_BOOK, 1, achievement.getName(), ChatColor.WHITE + "" + ChatColor.BOLD + "COMPLETED!"));
				start++;
			}else{
				inventory.setItem(start, Items.getInstance.create(Material.PAPER, 1, achievement.getName(), achievement.description()));
				start++;
			}
		}
	
		for(Achievement achievement : Install.getInstance().getaManager().blockPlaceAchievements){
			if(achievement.playerHas(p)){
				inventory.setItem(start, Items.getInstance.create(Material.ENCHANTED_BOOK, 1, achievement.getName(), ChatColor.WHITE + "" + ChatColor.BOLD + "COMPLETED!"));
				start++;
			}else{
				inventory.setItem(start, Items.getInstance.create(Material.PAPER, 1, achievement.getName(), achievement.description()));
				start++;
			}
		}

		for(Achievement achievement : Install.getInstance().getaManager().entityAchievements){
			if(achievement.playerHas(p)){
				inventory.setItem(start, Items.getInstance.create(Material.ENCHANTED_BOOK, 1, achievement.getName(), ChatColor.WHITE + "" + ChatColor.BOLD + "COMPLETED!"));
				start++;
			}else{
				inventory.setItem(start, Items.getInstance.create(Material.PAPER, 1, achievement.getName(), achievement.description()));
				start++;
			}
		}
		
		for(Achievement achievement : Install.getInstance().getaManager().walkAchievements){
			if(achievement.playerHas(p)){
				inventory.setItem(start, Items.getInstance.create(Material.ENCHANTED_BOOK, 1, achievement.getName(), ChatColor.WHITE + "" + ChatColor.BOLD + "COMPLETED!"));
				start++;
			}else{
				inventory.setItem(start, Items.getInstance.create(Material.PAPER, 1, achievement.getName(), achievement.description()));
				start++;
			}
		}
		
		p.openInventory(inventory);
	
	}

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		
		Player player = (Player) sender;
		player.closeInventory();
		
		onOpen(player);
		
		return false;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		if(e.getInventory().getTitle().contains(inventory.getTitle())){
			if(e.getCurrentItem() == null){ return; }
			if(e.getSlotType() == SlotType.OUTSIDE){ return; }
			
			e.setCancelled(true);
		}
	}
	
}
