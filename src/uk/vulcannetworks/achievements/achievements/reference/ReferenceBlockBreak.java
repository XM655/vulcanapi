package uk.vulcannetworks.achievements.achievements.reference;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import uk.vulcannetworks.achievements.interfaces.BlockBreakAchievement;
import uk.vulcannetworks.achievements.interfaces.superclass.AchievementType;
import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class ReferenceBlockBreak implements BlockBreakAchievement{

	@Override
	public String getName() {
		return "Become a Griefer!";
	}

	@Override
	public int getId() {
		return 2;
	}

	@Override
	public boolean completed() {
		return false;
	}

	@Override
	public boolean playerHas(Player p) {
		return bPlugin.getInstance().getConfig().getBoolean("player." + p.getUniqueId().toString() + "." + getId() + ".has");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void action(Player p, Block b, boolean isCancelled) {
		if(isCancelled) return;
		
		if(playerHas(p)) return;
		
		int concurrent = bPlugin.getInstance().getConfig().getInt("player." + p.getUniqueId().toString() + "." + getId() + ".count");
		
		if(concurrent > 499){
			p.sendTitle(ChatColor.GRAY + "Achievement Completed!", ChatColor.GOLD + "'Become a Griefer!'");
			bPlugin.getInstance().getConfig().set("player." + p.getUniqueId().toString() + "." + getId() + ".has", true);
			for(Player player : Bukkit.getOnlinePlayers()){
				Install.getInstance().getvAPI().sendAb(player, ChatColor.RED + "" + ChatColor.UNDERLINE + p.getName() + ChatColor.RED + " has completed " + ChatColor.UNDERLINE + "'Become a Griefer'!");
			}
			return;
		}
		
		concurrent = concurrent + 1;
		bPlugin.getInstance().getConfig().set("player." + p.getUniqueId().toString() + "." + getId() + ".count", concurrent);
		Install.getInstance().getvAPI().sendAb(p,"Become a Griefer | Score: " + concurrent);
		

	}

	@Override
	public AchievementType type() {
		return AchievementType.BLOCK_BREAK;
	}

	@Override
	public String description() {
		return "Grief a total of 500 Blocks!";
	}

}
