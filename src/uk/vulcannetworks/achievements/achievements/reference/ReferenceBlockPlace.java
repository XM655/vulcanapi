package uk.vulcannetworks.achievements.achievements.reference;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import uk.vulcannetworks.achievements.interfaces.BlockPlaceAchievement;
import uk.vulcannetworks.achievements.interfaces.superclass.AchievementType;
import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class ReferenceBlockPlace implements BlockPlaceAchievement{

	@Override
	public String getName() {
		return "Become a Builder!";
	}

	@Override
	public int getId() {
		return 1;
	}

	@Override
	public boolean completed() {
		return false;
	}

	@Override
	public boolean playerHas(Player p) {
		return bPlugin.getInstance().getConfig().getBoolean("player." + p.getUniqueId().toString() + "." + getId() + ".has");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void action(Player p, Block b, boolean isCancelled) {
		if(isCancelled) return;
		
		if(playerHas(p)) return;
		
		int concurrent = bPlugin.getInstance().getConfig().getInt("player." + p.getUniqueId().toString() + "." + getId() + ".count");
		
		if(concurrent > 499){
			p.sendTitle(ChatColor.GRAY + "Achievement Completed!", ChatColor.GOLD + "'Become a Builder!'");
			bPlugin.getInstance().getConfig().set("player." + p.getUniqueId().toString() + "." + getId() + ".has", true);
			for(Player player : Bukkit.getOnlinePlayers()){
				Install.getInstance().getvAPI().sendAb(player, ChatColor.RED + "" + ChatColor.UNDERLINE + p.getName() + ChatColor.RED + " has completed " + ChatColor.UNDERLINE + "'Become a Builder'!");
			}
			return;
		}
		
		concurrent = concurrent + 1;
		bPlugin.getInstance().getConfig().set("player." + p.getUniqueId().toString() + "." + getId() + ".count", concurrent);
		Install.getInstance().getvAPI().sendAb(p,"Become a Builder | Score: " + concurrent); //DEBUG
		

	}

	@Override
	public AchievementType type() {
		return AchievementType.BLOCK_PLACE;
	}

	@Override
	public String description() {
		return "Place a total of 500 Blocks!";
	}

}
