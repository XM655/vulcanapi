package uk.vulcannetworks.achievements.achievements.reference;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import uk.vulcannetworks.achievements.interfaces.EntityAchievement;
import uk.vulcannetworks.achievements.interfaces.superclass.AchievementType;
import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class HitTheDeveloperAchievement implements EntityAchievement{

	@Override
	public String getName() {
		return "Hit the Developer";
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return "Find a wild XM655 and punch him!";
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public boolean completed() {
		return false;
	}

	@Override
	public AchievementType type() {
		// TODO Auto-generated method stub
		return AchievementType.ENTITY;
	}

	@Override
	public boolean playerHas(Player p) {
		return bPlugin.getInstance().getConfig().getBoolean("player." + p.getUniqueId().toString() + "." + getId() + ".has");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void action(Player p, Entity e, boolean isCancelled) {
		if(isCancelled) return;
		
		if(playerHas(p)) return;
		
		if(e instanceof Player){
			Player hit = (Player) e;
			
			if(hit.getName().equalsIgnoreCase("XM655")){
				p.sendTitle(ChatColor.GRAY + "Achievement Completed!", ChatColor.GOLD + "'Hit a Developer!'");
				bPlugin.getInstance().getConfig().set("player." + p.getUniqueId().toString() + "." + getId() + ".has", true);
				for(Player player : Bukkit.getOnlinePlayers()){
					Install.getInstance().getvAPI().sendAb(player, ChatColor.RED + "" + ChatColor.UNDERLINE + p.getName() + ChatColor.RED + " has completed " + ChatColor.UNDERLINE + "'Hit a Developer'!");
				}
			}
			
		}
		
	}

}
