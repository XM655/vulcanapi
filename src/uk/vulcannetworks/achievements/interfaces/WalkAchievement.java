package uk.vulcannetworks.achievements.interfaces;

import org.bukkit.entity.Player;

import uk.vulcannetworks.achievements.interfaces.superclass.Achievement;

public interface WalkAchievement extends Achievement{

	public void action(Player p);
	
}
