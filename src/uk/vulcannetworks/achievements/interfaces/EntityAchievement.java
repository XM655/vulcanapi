package uk.vulcannetworks.achievements.interfaces;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import uk.vulcannetworks.achievements.interfaces.superclass.Achievement;

public interface EntityAchievement extends Achievement{

	public void action(Player p, Entity e, boolean isCancelled);
	
}
