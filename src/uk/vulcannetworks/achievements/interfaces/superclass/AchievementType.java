package uk.vulcannetworks.achievements.interfaces.superclass;

public enum AchievementType {

	BLOCK_BREAK, BLOCK_PLACE, ENTITY, WALK;
	
}
