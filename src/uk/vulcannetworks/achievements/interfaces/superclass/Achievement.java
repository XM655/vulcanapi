package uk.vulcannetworks.achievements.interfaces.superclass;

import org.bukkit.entity.Player;

public interface Achievement {

	public String getName();
	public String description();
	public int getId();
	public boolean completed();
	
	public AchievementType type();
	
	
	public boolean playerHas(Player p);
	
}
