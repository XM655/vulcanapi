package uk.vulcannetworks.achievements.interfaces;

import org.bukkit.entity.Player;

import org.bukkit.block.Block;
import uk.vulcannetworks.achievements.interfaces.superclass.Achievement;

public interface BlockBreakAchievement extends Achievement{

	public void action(Player p, Block b, boolean isCancelled);
	
}
