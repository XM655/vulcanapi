package uk.vulcannetworks.achievements;


import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import uk.vulcannetworks.achievements.interfaces.BlockBreakAchievement;
import uk.vulcannetworks.achievements.interfaces.BlockPlaceAchievement;
import uk.vulcannetworks.achievements.interfaces.EntityAchievement;
import uk.vulcannetworks.achievements.interfaces.WalkAchievement;

public class AchievementsManager implements Listener{

	public ArrayList<BlockBreakAchievement> blockBreakAchievements = new ArrayList<>();
	public ArrayList<BlockPlaceAchievement> blockPlaceAchievements = new ArrayList<>();
	public ArrayList<EntityAchievement> entityAchievements = new ArrayList<>();
	public ArrayList<WalkAchievement> walkAchievements = new ArrayList<>();

	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent bbe){
		for(BlockBreakAchievement bba : blockBreakAchievements){
			bba.action(bbe.getPlayer(), bbe.getBlock(), bbe.isCancelled());
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent bpe){
		for(BlockPlaceAchievement bpa : blockPlaceAchievements){
			bpa.action(bpe.getPlayer(), bpe.getBlock(), bpe.isCancelled());
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent pme){
		if((pme.getFrom().getX() != pme.getTo().getX()) || (pme.getFrom().getZ() != pme.getTo().getZ())){
			for(WalkAchievement wa : walkAchievements){
				wa.action(pme.getPlayer());
			}
		}
	}
	
	@EventHandler
	public void onPlayerDamageEntity(EntityDamageByEntityEvent edbee){
		if(edbee.getDamager() instanceof Player){
			for(EntityAchievement ea : entityAchievements){
				ea.action(((Player)edbee.getDamager()), edbee.getEntity(), edbee.isCancelled());
			}
		}
	}
	
}
