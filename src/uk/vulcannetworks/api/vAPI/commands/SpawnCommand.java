package uk.vulcannetworks.api.vAPI.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class SpawnCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		if(!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "Beep-bop, you are not a Player, Mr. Robot!");
			return true;
		}
		
		Player player = (Player) sender;
		
		if(!(player.hasPermission("vulcannetworks.admin"))){ Install.getInstance().getvAPI().send(player, "You are lacking required permissions."); return true;}

		Install.getInstance().getvAPI().setSpawn(player.getWorld(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ());
		Install.getInstance().getvAPI().send(player, "Spawn successfully set.");
		
		return true;
	}

}
