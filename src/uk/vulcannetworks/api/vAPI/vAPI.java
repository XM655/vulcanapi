package uk.vulcannetworks.api.vAPI;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import uk.vulcannetworks.bukkit.bPlugin;

public class vAPI {

	private String prefix = translate("&8&l>> &f", '&');
	private Location spawn = null;
	
	public void send(Player p, String message){
		p.sendMessage(prefix + translate(message, '&'));
	}
	
	public void send(Player p, ChatColor chatColor, String message){
		p.sendMessage(prefix + chatColor + translate(message, '&'));
	}
	
	public void sendAb(Player p, String messageActionBar){
		IChatBaseComponent chatBaseComponent = ChatSerializer.a("{\"text\": \"" + messageActionBar + "\"}");
		PacketPlayOutChat packetPlayOutChat = new PacketPlayOutChat(chatBaseComponent, (byte) 2);
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(packetPlayOutChat);
	}
	
	public String translate(String message, char replacing){
		return ChatColor.translateAlternateColorCodes(replacing, message);
	}
	
	public String getPrefix(){
		return prefix;
	}
	
	public void sendPlayer(Player p, String server){
		send(p, "Sending you to " + ChatColor.UNDERLINE + server);
		
		try{
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			DataOutputStream outputStream = new DataOutputStream(byteArrayOutputStream);
			outputStream.writeUTF("Connect");
			outputStream.writeUTF(server);
			p.sendPluginMessage(bPlugin.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray());
			byteArrayOutputStream.close();
			outputStream.close();
		}catch(Exception e){
			send(p, "Error during server teleportation!");
			return;
		}
		
	}
	
	public Location getSpawn(){
		return spawn;
	}
	
	public void setSpawn(Location l){
		setSpawn(l.getWorld(), l.getX(), l.getY(), l.getZ());
	}
	
	public void setSpawn(org.bukkit.World w, double x, double y, double z){
		spawn = new Location(w, x, y, z);
		bPlugin.getInstance().getConfig().set("worlds.spawn.world", w.getName());
		bPlugin.getInstance().getConfig().set("worlds.spawn.x", x);
		bPlugin.getInstance().getConfig().set("worlds.spawn.y", y);
		bPlugin.getInstance().getConfig().set("worlds.spawn.z", z);
		bPlugin.getInstance().saveConfig();
	}
	
}
