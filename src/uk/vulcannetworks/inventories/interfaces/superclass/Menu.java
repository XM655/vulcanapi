package uk.vulcannetworks.inventories.interfaces.superclass;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class Menu {

	public Inventory inventory;
	
	public Menu(String name, int size){
		inventory = Bukkit.createInventory(null, size, name);
		registerItems();
	}
	
	public Menu(String name, InventoryType inventoryType){
		inventory = Bukkit.createInventory(null, inventoryType, name);
		registerItems();
	}
	
	public abstract void registerItems();
	public abstract void click(ItemStack is, Player p);
	
	
}
