package uk.vulcannetworks.inventories;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;

import uk.vulcannetworks.inventories.interfaces.superclass.Menu;

public class MenuManager implements Listener{

	private Map<String, Menu> menus = new HashMap<>();
	
	public void registerMenu(String id, Menu menu){
		menus.put(id, menu);
	}
	
	public Menu getMenu(String id){
		return menus.get(id);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		if((e.getSlotType() == SlotType.OUTSIDE) || !(e.getSlot() > -1) || (e.getCurrentItem() == null) || (e.getCurrentItem().getType() == Material.AIR)) return;
		
		for(Menu menu : menus.values()){
			if(menu.inventory.getTitle().contains(e.getInventory().getTitle())){
				menu.click(e.getCurrentItem(), ((Player) e.getWhoClicked()));
				e.setResult(Result.DENY);
				e.setCancelled(true);
			}
		}
		
	}
	
	
}
