package uk.vulcannetworks.permissions.ranks;

import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class RankCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if(!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "Non-player objects can issue this command!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if(args.length == 0){
			sendRank(p);
			return true;
		}
		
		if((args.length == 2) && (p.hasPermission("vulcannetworks.admin"))){
			if(args[0].equalsIgnoreCase("admin")){
				if(args[1].equalsIgnoreCase("reload")){
					bPlugin.getInstance().reloadConfig();
					Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "Reloaded 'config.yml' | Ingame permissions have not been reloaded!");
					return true;
				}
			}
		}
		
		if((args.length > 3) && (p.hasPermission("vulcannetworks.admin"))){
			if(args[0].equalsIgnoreCase("admin")){
				if(args[1].equalsIgnoreCase("set")){
					if(Bukkit.getPlayer(args[2]) != null){
						try {
							Install.getInstance().getvSQL().update("rank", "user", Bukkit.getPlayer(args[2]).getUniqueId().toString(), args[3].toUpperCase());
							Install.getInstance().getvPerms().setupPlayer(Bukkit.getPlayer(args[2]));
							Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "User '" + args[2] + "' has been updated to '" + args[3].toUpperCase() + "'!");
							return true;
						} catch (SQLException e) {
							Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "User '" + args[2] + "' has NOT been updated to '" + args[3].toUpperCase() + "'!");
							e.printStackTrace();
							return true;
						}
				}else{
						Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "User '" + args[2] + "' has NOT been updated to '" + args[3].toUpperCase() + "' due to being missing!");
					return true;
				}
				}
				
				if(args[1].equalsIgnoreCase("create")){
					if(args[2].equalsIgnoreCase("group")){

						FileConfiguration fConfiguration = bPlugin.getInstance().getConfig();
						
						if(fConfiguration.getString(args[3].toUpperCase()) != null){
							Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "Group '" + args[3].toUpperCase() + "' has NOT been created due to being existing!");
							return true;
						}else{
							ArrayList<String> perms = new ArrayList<String>();
							fConfiguration.set(args[3].toUpperCase() + ".prefix", args[3]);
							fConfiguration.set(args[3].toUpperCase() + ".perms", perms);
							Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "Group '" + args[3].toUpperCase() + "' has been created!");
							bPlugin.getInstance().saveConfig();
							bPlugin.getInstance().reloadConfig();
							return true;
						}
						
					}
				}
				
				
				if(args[1].equalsIgnoreCase("create-add")){
						FileConfiguration fConfiguration = bPlugin.getInstance().getConfig();
						
						if(fConfiguration.getString(args[2].toUpperCase()) != null){
							
							java.util.List<String> perms = fConfiguration.getStringList(args[2].toUpperCase() + ".perms");
							perms.add(args[3]);
							
							fConfiguration.set(args[2].toUpperCase() + ".perms", perms);

							Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "Group '" + args[2].toUpperCase() + "has been given '" + args[3] + "'!");

							bPlugin.getInstance().saveConfig();
							bPlugin.getInstance().reloadConfig();
							
							return true;
						}else{
							Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "Group '" + args[2].toUpperCase() + "' has NOT been altered due to not being existing!");
							return true;
						}
						
					
				}
				
				if(args[1].equalsIgnoreCase("create-remove")){
					FileConfiguration fConfiguration = bPlugin.getInstance().getConfig();
					
					if(fConfiguration.getString(args[2].toUpperCase()) != null){
						
						java.util.List<String> perms = fConfiguration.getStringList(args[2].toUpperCase() + ".perms");
						
						int counts = 0;
						
						while(perms.iterator().hasNext()){
							if(perms.iterator().next().equalsIgnoreCase(args[3])){
								counts = counts + 1;
								perms.remove(perms.iterator().next());
							}
						}
						
						fConfiguration.set(args[2].toUpperCase() + ".perms", perms);

						Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "Removed permissions from group. " + ChatColor.ITALIC + "Removed (" + counts + ") entries!");

						bPlugin.getInstance().saveConfig();
						bPlugin.getInstance().reloadConfig();
						
						return true;
					}else{
						Install.getInstance().getvAPI().send(p, ChatColor.GRAY + "Group '" + args[2].toUpperCase() + "' has NOT been altered due to not being existing!");
						return true;
					}
				}
			
				
			}
			
		}
		
		return false;
	}
	
	public void sendRank(Player p){
		p.sendMessage(ChatColor.LIGHT_PURPLE + "------ <" + ChatColor.GRAY + "RANK" + ChatColor.LIGHT_PURPLE + "> ------");
		p.sendMessage("");
		try {
			p.sendMessage(ChatColor.GRAY + "Your rank is: " + ChatColor.GOLD + Install.getInstance().getvSQL().readDataString("user", "rank", p.getUniqueId().toString()));
		} catch (SQLException e) {
			p.sendMessage(ChatColor.GRAY + "Error during collection period, contact a moderator as soon as possible.");

			e.printStackTrace();
		}
		p.sendMessage("");
		p.sendMessage(ChatColor.LIGHT_PURPLE + "------ <" + ChatColor.GRAY + "RANK" + ChatColor.LIGHT_PURPLE + "> ------");
	}

}
