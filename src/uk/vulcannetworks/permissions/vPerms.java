package uk.vulcannetworks.permissions;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class vPerms implements Listener{
	
	private HashMap<UUID, PermissionAttachment> pHashMap = new HashMap<>();
	
	public PermissionAttachment getAttachment(UUID uuid){
		return pHashMap.get(uuid);
	}
	
	public PermissionAttachment getAttachment(Player player){
		return pHashMap.get(player.getUniqueId());
	}
	
	public void putAttachment(Player p, PermissionAttachment permissionAttachment){
		pHashMap.put(p.getUniqueId(), permissionAttachment);
	}
	
	public void removeFromAttach(Player p){
		if(pHashMap.containsKey(p.getUniqueId())){
			pHashMap.remove(p.getUniqueId());
		}else{
			return;
		}
	}
	
	public void setupPlayer(Player p){
		PermissionAttachment pAttachment = p.addAttachment(bPlugin.getInstance());
		try {
			String rank = Install.getInstance().getvSQL().readDataString("user", "rank", p.getUniqueId().toString());
			
			FileConfiguration fConfiguration = bPlugin.getInstance().getConfig();
			
			java.util.List<String> getPerms = fConfiguration.getStringList(rank + ".perms");
			
			for(String string : getPerms){
				pAttachment.setPermission(string, true);
			}
			
			putAttachment(p, pAttachment);

			Install.getInstance().getvChat().putPlayer(p.getName(), fConfiguration.getString(rank + ".prefix"));
			
			return;
			
		} catch (SQLException e) {
			p.kickPlayer("A issue has occured with our MySQL Database.");
			e.printStackTrace();
		}
		
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		try {
			if(Install.getInstance().getvSQL().contains("user", e.getPlayer().getUniqueId().toString())){
			   setupPlayer(e.getPlayer());
			}else{
				Install.getInstance().getvSQL().insertData("user", e.getPlayer().getUniqueId().toString(), "DEFAULT", 1);
				setupPlayer(e.getPlayer());
			}
		} catch (SQLException e1) {
			e.getPlayer().kickPlayer("A issue has occured with our MySQL Database.");
			e1.printStackTrace();
		}
	}
	
}
