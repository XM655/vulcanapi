package uk.vulcannetworks.superhero;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerJoinEvent;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.superhero.actions.BatShield;
import uk.vulcannetworks.superhero.actions.SuperFlight;
import uk.vulcannetworks.superhero.actions.WebShoot;
import uk.vulcannetworks.superhero.actions.WonderStrength;
import uk.vulcannetworks.superhero.heros.Batman;
import uk.vulcannetworks.superhero.heros.Spiderman;
import uk.vulcannetworks.superhero.heros.Superman;
import uk.vulcannetworks.superhero.heros.Wonderwoman;
import uk.vulcannetworks.superhero.menu.SuperHeroMenu;

public class SuperHeroListener implements Listener{
	
	private SuperHeroMenu shp = new SuperHeroMenu(ChatColor.GRAY + "SuperHero Selection Menu", InventoryType.HOPPER);
	
	public void runOnce(){
		shp.registerItems();
		Install.getInstance().getMenuManager().registerMenu("SuperHero", shp);
		Bukkit.getPluginManager().registerEvents(new WebShoot(), bPlugin.getInstance());
		Bukkit.getPluginManager().registerEvents(new WonderStrength(), bPlugin.getInstance());
		Bukkit.getPluginManager().registerEvents(new BatShield(), bPlugin.getInstance());
		Bukkit.getPluginManager().registerEvents(new SuperFlight(), bPlugin.getInstance());
		
		
		for(Player player : Bukkit.getOnlinePlayers()){
			try {
				String hero = Install.getInstance().getvSQL().readDataString("user", "superhero", player.getUniqueId().toString());
				int level = Install.getInstance().getvSQL().readDataInt("user", "level", player.getUniqueId().toString());

				Install.getInstance().getSuperHeroManager().getRawLevels().put(player.getName(), level);
				
				if(hero == null){
					Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable() {
						@Override
						public void run() {
							player.openInventory(shp.inventory);
						}
					}, 20);
					return;
				}		
				assignPlayer(player, hero);
				} catch (Exception e1) {
					player.kickPlayer("A issue has occured with our MySQL Database.");
					e1.printStackTrace();
				}
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		try {
			String hero = Install.getInstance().getvSQL().readDataString("user", "superhero", e.getPlayer().getUniqueId().toString());
			int level = Install.getInstance().getvSQL().readDataInt("user", "level", e.getPlayer().getUniqueId().toString());

			Install.getInstance().getSuperHeroManager().getRawLevels().put(e.getPlayer().getName(), level);
		
		if(hero == null){
			Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable() {
				@Override
				public void run() {
					e.getPlayer().openInventory(shp.inventory);
				}
			}, 20);
			return;
		}			
		
		assignPlayer(e.getPlayer(), hero);


		Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable() {
			@Override
			public void run() {
				if((Install.getInstance().getAbilityManager().getPlayer(e.getPlayer()).getAbilities().contains(new SuperFlight()))){
					e.getPlayer().setAllowFlight(true);
				}
			}
		  }, 40L);
		
		} catch (Exception e1) {
			e.getPlayer().kickPlayer("A issue has occured with our MySQL Database.");
			e1.printStackTrace();
		}
		
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e){
		if(e.getInventory().getTitle().contains(shp.inventory.getTitle())){
			if(Install.getInstance().getSuperHeroManager().contains(((Player)e.getPlayer()))){
				Install.getInstance().getvAPI().send(((Player)e.getPlayer()), "You have successfully became a hero!");
				Install.getInstance().getSuperHeroManager().giveLevel(((Player) e.getPlayer()), 25);
				return;
			}
				Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable() {
					@Override
					public void run() {
						e.getPlayer().openInventory(e.getInventory());
					}
				}, 1);
			Install.getInstance().getvAPI().send(((Player)e.getPlayer()), ChatColor.RED + "You must become a hero!");
		}
	}
	
	public void assignPlayer(Player p, String hero){
		if(hero.equalsIgnoreCase("superman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Superman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become SuperMan!");
			return;
		}
		
		if(hero.equalsIgnoreCase("spiderman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Spiderman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become SpiderMan!");
			return;
		}
		
		if(hero.equalsIgnoreCase("batman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Batman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become BatMan!");
			return;
		}
		
		if(hero.equalsIgnoreCase("wonderwoman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Wonderwoman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become Wonderwoman!");
			return;
		}
	}
	
	
	
}
