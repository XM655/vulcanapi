package uk.vulcannetworks.superhero;

import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.superhero.superclass.SuperHero;

public class SuperheroManager {

	private HashMap<String, SuperHero> superHeros = new HashMap<String, SuperHero>();
	private HashMap<String, Integer> levels = new HashMap<String, Integer>();
	
	public HashMap<String, SuperHero> getRawSuperHeros(){
		return superHeros;
	}
	
	public HashMap<String, Integer> getRawLevels(){
		return levels;
	}
	
	public void giveLevel(Player p, int i){
		int c = i;
		i = levels.get(p.getName()) + i;
		try {
			Install.getInstance().getvSQL().update("level", "user", p.getUniqueId().toString(), i);
			Install.getInstance().getvAPI().send(p, "You have been given " + ChatColor.GOLD + c + ChatColor.WHITE + " Hero XP!");
			levels.remove(p.getName());
			levels.put(p.getName(), i);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int getLevel(Player p){
		return levels.get(p.getName());
	}
	
	public void addPlayer(Player p, SuperHero sh){
		superHeros.put(p.getName(), sh);
	}
	
	public void removePlayer(Player p){
		superHeros.remove(p.getName());
	}
	
	public SuperHero getPlayersHero(Player p){
		return superHeros.get(p.getName());
	}
	
	public boolean contains(Player p){
		return superHeros.containsKey(p.getName());
	}

}
