package uk.vulcannetworks.superhero;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.sql.vSQL;
import uk.vulcannetworks.superhero.ability.Ability;
import uk.vulcannetworks.superhero.ability.player.HeroPlayer;
import uk.vulcannetworks.superhero.actions.BatShield;
import uk.vulcannetworks.superhero.actions.SuperFlight;
import uk.vulcannetworks.superhero.actions.WebShoot;
import uk.vulcannetworks.superhero.actions.WonderStrength;

public class AbilityManager implements Listener{

	private Map<String, HeroPlayer> heros = new HashMap<String, HeroPlayer>();
	private Map<Integer, Ability> abilities = new HashMap<Integer, Ability>();
	
	public Ability getAbilityById(int id){
		return abilities.get(id);
	}
	
	public void put(Ability i, int id){
		abilities.put(id, i);
	}
	
	public void putDefaults(){
		put(new BatShield(), 1);
		put(new SuperFlight(), 2);
		put(new WebShoot(), 3);
		put(new WonderStrength(), 4);
	}
	
	public HeroPlayer getPlayer(Player p){
		for(String heroPlayer : heros.keySet()){
			if(heros.get(heroPlayer).getPlayer().getName() == p.getName()){
				return heros.get(heroPlayer);
			}
		}
		return null;
	}
	
	public void put(Player p){		
		heros.put(p.getName(), new HeroPlayer(p, new ArrayList<Ability>()));
	}
	
	public void wipe(Player p){
		if(contains(p)){
			heros.remove(p.getName());
		}
	}
	
	public boolean contains(Player p){
		for(String heroPlayer : heros.keySet()){
			if(heros.get(heroPlayer).getPlayer().getName() == p.getName()){
				return true;
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		put(e.getPlayer());
		try {
			HeroPlayer hp = getPlayer(e.getPlayer());
			hp.setAbilities(loadAll(e.getPlayer()));
			
			wipe(e.getPlayer());
			heros.put(e.getPlayer().getName(), hp);
		} catch (SQLException e1) {
			e.getPlayer().sendTitle(ChatColor.RED + "! WARNING !", ChatColor.GRAY + "We was unable to load your abilities!");
		}
	}
	
	public ArrayList<Ability> loadAll(Player p) throws SQLException{
		vSQL sql = Install.getInstance().getvSQL();
		Statement statement = sql.getRAW().createStatement();
		String statementSQL = "SELECT * FROM abilities WHERE uuid" + "='" + p.getUniqueId().toString() + "';";
		ResultSet resultSet = statement.executeQuery(statementSQL);
		
		ArrayList<Ability> list = new ArrayList<Ability>();
		
		while(resultSet.next()){
			list.add(getAbilityById(resultSet.getInt("actionId")));
		}
		
		if(list.contains(getAbilityById(2))) p.setAllowFlight(true);
		
		return list;
		
	}
	
}
