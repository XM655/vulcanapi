package uk.vulcannetworks.superhero.superclass;

public interface SuperHero {

	String name();
	
	String displayName();
}
