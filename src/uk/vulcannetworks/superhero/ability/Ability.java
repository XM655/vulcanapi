package uk.vulcannetworks.superhero.ability;

public interface Ability {

	int id();
	String name();
	
}
