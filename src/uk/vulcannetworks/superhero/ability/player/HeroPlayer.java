package uk.vulcannetworks.superhero.ability.player;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import uk.vulcannetworks.superhero.ability.Ability;

public class HeroPlayer {

	private Player player;
	private ArrayList<Ability> abilities = new ArrayList<>();
	
	public HeroPlayer(Player player, ArrayList<Ability> abilities) {
		super();
		this.player = player;
		this.abilities = abilities;
	}
	
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public ArrayList<Ability> getAbilities() {
		return abilities;
	}
	public void setAbilities(ArrayList<Ability> abilities) {
		this.abilities = abilities;
	}
	
	
	
}
