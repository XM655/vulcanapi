package uk.vulcannetworks.superhero.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;

public class LevelCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if(!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "Non-player objects can issue this command!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if(args.length == 0){
			sendRank(p);
			return true;
		}
		
		return true;
		
	}
	
	
	public void sendRank(Player p){
		p.sendMessage(ChatColor.LIGHT_PURPLE + "------ <" + ChatColor.GRAY + "HERO XP" + ChatColor.LIGHT_PURPLE + "> ------");
		p.sendMessage("");
			p.sendMessage(ChatColor.GRAY + "You have: " + ChatColor.GOLD + Install.getInstance().getSuperHeroManager().getRawLevels().get(p.getName()) + " Hero XP!");
		p.sendMessage("");
		p.sendMessage(ChatColor.LIGHT_PURPLE + "------ <" + ChatColor.GRAY + "HERO XP" + ChatColor.LIGHT_PURPLE + "> ------");
	}
	
}
