package uk.vulcannetworks.superhero.menu;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.hub.utils.Items;
import uk.vulcannetworks.inventories.interfaces.superclass.Menu;
import uk.vulcannetworks.superhero.heros.Batman;
import uk.vulcannetworks.superhero.heros.Spiderman;
import uk.vulcannetworks.superhero.heros.Superman;
import uk.vulcannetworks.superhero.heros.Wonderwoman;

public class SuperHeroMenu extends Menu{

	public SuperHeroMenu(String name, InventoryType inventoryType) {
		super(name, inventoryType);
	}

	@Override
	public void registerItems() {
		inventory.addItem(Items.getInstance.create(Material.SKULL_ITEM, 1, ChatColor.GRAY + "Batman", ""));
		inventory.addItem(Items.getInstance.create(Material.SKULL_ITEM, 1, ChatColor.GRAY + "Spiderman", ""));
		inventory.addItem(Items.getInstance.create(Material.SKULL_ITEM, 1, ChatColor.GRAY + "Superman", ""));
		inventory.addItem(Items.getInstance.create(Material.SKULL_ITEM, 1, ChatColor.GRAY + "Wonderwoman", ""));		
	}

	@Override
	public void click(ItemStack is, Player p) {
	
		String string = is.getItemMeta().getDisplayName();

		try {
			if(string.contains("Batman")){
				Install.getInstance().getvSQL().update("superhero", "user", p.getUniqueId().toString(), "batman");
					assignPlayer(p, "batman");
					p.closeInventory();

			}
			
			if(string.contains("Spiderman")){
				Install.getInstance().getvSQL().update("superhero", "user", p.getUniqueId().toString(), "spiderman");
				assignPlayer(p, "spiderman");
				p.closeInventory();

			}
			
			if(string.contains("Superman")){
				Install.getInstance().getvSQL().update("superhero", "user", p.getUniqueId().toString(), "superman");
				assignPlayer(p, "superman");
				p.closeInventory();

			}
			if(string.contains("Wonderwoman")){
				Install.getInstance().getvSQL().update("superhero", "user", p.getUniqueId().toString(), "wonderwoman");
				assignPlayer(p, "wonderwoman");
				p.closeInventory();

		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void assignPlayer(Player p, String hero){
		Bukkit.broadcastMessage(hero);
		if(hero.equalsIgnoreCase("superman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Superman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become SuperMan!");
			return;
		}
		
		if(hero.equalsIgnoreCase("spiderman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Spiderman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become SpiderMan!");
			return;
		}
		
		if(hero.equalsIgnoreCase("batman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Batman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become BatMan!");
			return;
		}
		
		if(hero.equalsIgnoreCase("wonderwoman")){
			Install.getInstance().getSuperHeroManager().addPlayer(p, new Wonderwoman());
			Install.getInstance().getvAPI().sendAb(p, ChatColor.GRAY + "You have become Wonderwoman!");
			return;
		}
	}

}
