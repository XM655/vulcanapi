package uk.vulcannetworks.superhero.actions;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.superhero.ability.Ability;

public class WonderStrength implements Listener, Ability{

	private ArrayList<String> cooldown = new ArrayList<String>();
	
	@EventHandler
	public void onPlayerDamageEntity(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player){
			Player player = (Player) e.getDamager();	
			if((Install.getInstance().getAbilityManager().getPlayer(player).getAbilities().contains(Install.getInstance().getAbilityManager().getAbilityById(4))) && !(cooldown.contains(player.getName()))){
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 3 * 20, 1));
				cooldown.add(player.getName());
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable(){
					public void run(){
						cooldown.remove(player.getName());
						Install.getInstance().getvAPI().sendAb(player, ChatColor.GOLD + "WonderStrength refreshed.");
					}
				}, 15 * 20);
			}
		}
	}

	@Override
	public int id() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public String name() {
		return "WonderStrength";
	}
	
}
