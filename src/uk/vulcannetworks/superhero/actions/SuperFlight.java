package uk.vulcannetworks.superhero.actions;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.superhero.ability.Ability;

public class SuperFlight implements Listener, Ability{

	private ArrayList<String> players = new ArrayList<String>();
	
	@EventHandler
	public void onPlayerInteract(PlayerToggleFlightEvent e){
		if((e.getPlayer().getGameMode() != GameMode.CREATIVE) && !(players.contains(e.getPlayer().getName())) && (e.getPlayer().getAllowFlight()) && (Install.getInstance().getAbilityManager().getPlayer(e.getPlayer()).getAbilities().contains(Install.getInstance().getAbilityManager().getAbilityById(2)))){
			players.add(e.getPlayer().getName());
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable() {
				@Override
				public void run() {
					e.getPlayer().setAllowFlight(false);
					e.getPlayer().setFlying(false);
					e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 20));
				}
			}, 10 * 20);
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable() {
				@Override
				public void run() {
					players.remove(e.getPlayer().getName());
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_ANVIL_USE, 3, 3);
					Install.getInstance().getvAPI().sendAb(e.getPlayer(), ChatColor.GOLD + "SuperFlight refreshed.");
					e.getPlayer().setAllowFlight(true);
				}
			}, 50 * 20);
		}
	}

	@Override
	public int id() {
		return 2;
	}

	@Override
	public String name() {
		return "SuperFlight";
	}
	
	
}
