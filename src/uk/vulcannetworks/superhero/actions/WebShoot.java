package uk.vulcannetworks.superhero.actions;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.superhero.ability.Ability;

public class WebShoot implements Listener, Ability{

	private ArrayList<String> players = new ArrayList<String>();
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
			if(!(Install.getInstance().getSuperHeroManager().getPlayersHero(e.getPlayer()) == null) && (Install.getInstance().getAbilityManager().getPlayer(e.getPlayer()).getAbilities().contains(Install.getInstance().getAbilityManager().getAbilityById(3)))){
				if(players.contains(e.getPlayer().getName())) return;
				
				Player player = e.getPlayer();
				Item item = player.getWorld().dropItem(player.getEyeLocation(), new ItemStack(Material.WEB, 1));
				item.setVelocity(player.getEyeLocation().getDirection().multiply(4));
				item.setPickupDelay(10000);
				counter(e.getPlayer(), item);
				players.add(e.getPlayer().getName());
				
			}
	}
	
	
	public void counter(Player p, Item item){
		new BukkitRunnable() {
			
			int counter = 0;
			
			@Override
			public void run() {
				if(counter > 25){
					item.remove();
					players.remove(p.getName());
					p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 3, 3);
					Install.getInstance().getvAPI().sendAb(p, ChatColor.GOLD + "SpiderWeb refreshed.");
					this.cancel();
				}
				
				List<Entity> nearby = item.getNearbyEntities(2, 2, 2);
				
				for(Entity entity : nearby){
					if(entity instanceof Player){
						Player player = (Player) entity;
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 45, 1));
					}
				}
				
				counter++;
			}
		}.runTaskTimer(bPlugin.getInstance(), 5, 5);
	}


	@Override
	public int id() {
		return 3;
	}


	@Override
	public String name() {
		// TODO Auto-generated method stub
		return "WebShoot";
	}
	
}
