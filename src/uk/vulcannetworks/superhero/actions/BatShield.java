package uk.vulcannetworks.superhero.actions;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.hub.utils.Install;
import uk.vulcannetworks.superhero.ability.Ability;

public class BatShield implements Listener, Ability{

	private ArrayList<String> players = new ArrayList<String>();
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		if((e.getAction() == Action.LEFT_CLICK_AIR) &&
				!(players.contains(e.getPlayer().getName())) &&
				(Install.getInstance().getAbilityManager().getPlayer(e.getPlayer()).getAbilities().contains(Install.getInstance().getAbilityManager().getAbilityById(1))) && (e.getPlayer().isSneaking())){
			counter(e.getPlayer());
			
			players.add(e.getPlayer().getName());
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(bPlugin.getInstance(), new Runnable() {
				@Override
				public void run() {
					players.remove(e.getPlayer().getName());
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_ANVIL_USE, 3, 3);
					Install.getInstance().getvAPI().sendAb(e.getPlayer(), ChatColor.GOLD + "BatShield refreshed.");
				}
			}, 35 * 20);
		}
	}
	
	public void counter(Player p){
		new BukkitRunnable() {
			
			int counter = 0;
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				
				if(!p.isSneaking()){
					p.sendTitle(ChatColor.BLACK + "Bat Shield De-Activated!", ChatColor.GOLD + "You must keep sneaking!");
					this.cancel();
					return;
				}
				
				if(counter > 25){
					p.sendTitle(ChatColor.BLACK + "Bat Shield De-Activated!", ChatColor.GOLD + "Your ability is cooling down.!");
					this.cancel();
					return;
				}
				
				p.sendTitle(ChatColor.BLACK + "Bat Shield Activated!", ChatColor.GOLD + "Players in 3x3x3 radius will be damaged!");
				

				
				List<Entity> nearby = p.getNearbyEntities(3, 3, 3);
				
				for(Entity entity : nearby){
					if(entity instanceof Player){
						Player player = (Player) entity;
						
						if(player.getName() == p.getName()){ return;}						
						player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 35, 1));
					}
				}
				
				counter++;
			}
		}.runTaskTimer(bPlugin.getInstance(), 5, 5);
	}

	@Override
	public int id() {
		return 1;
	}

	@Override
	public String name() {
		return "BatShield";
	}
}
