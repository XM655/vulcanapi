package uk.vulcannetworks.superhero.heros;

import uk.vulcannetworks.superhero.superclass.SuperHero;

public class Batman implements SuperHero{
	
	@Override
	public String name() {
		return "BatMan";
	}

	@Override
	public String displayName() {
		return null;
	}

}
