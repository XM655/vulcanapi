package uk.vulcannetworks.superhero.heros;

import uk.vulcannetworks.superhero.superclass.SuperHero;

public class Spiderman implements SuperHero{
	
	@Override
	public String name() {
		return "Spiderman";
	}

	@Override
	public String displayName() {
		return null;
	}

}
