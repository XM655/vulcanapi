package uk.vulcannetworks.superhero.heros;

import uk.vulcannetworks.superhero.superclass.SuperHero;

public class Wonderwoman implements SuperHero{
	
	@Override
	public String name() {
		return "WonderWoman";
	}

	@Override
	public String displayName() {
		return null;
	}
}
