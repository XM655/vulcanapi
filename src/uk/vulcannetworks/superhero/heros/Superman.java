package uk.vulcannetworks.superhero.heros;

import uk.vulcannetworks.superhero.superclass.SuperHero;

public class Superman implements SuperHero{
	
	@Override
	public String name() {
		return "Superman";
	}

	@Override
	public String displayName() {
		return null;
	}

}
