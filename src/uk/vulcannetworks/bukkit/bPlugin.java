package uk.vulcannetworks.bukkit;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import uk.vulcannetworks.hub.utils.Install;


public class bPlugin extends JavaPlugin{

	private static bPlugin instance;

	@Override
	public void onEnable(){
		instance = this;

		Install.getInstance().install();
	}

	public static bPlugin getInstance(){
	    return instance;
    }
}
