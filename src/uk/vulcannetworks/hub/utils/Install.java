package uk.vulcannetworks.hub.utils;


import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import uk.vulcannetworks.achievements.AchievementsManager;
import uk.vulcannetworks.achievements.achievements.reference.HitAAdminAchievement;
import uk.vulcannetworks.achievements.achievements.reference.HitTheDeveloperAchievement;
import uk.vulcannetworks.achievements.achievements.reference.ReferenceBlockBreak;
import uk.vulcannetworks.achievements.achievements.reference.ReferenceBlockPlace;
import uk.vulcannetworks.achievements.inventory.AchievementsInventory;
import uk.vulcannetworks.api.vAPI.vAPI;
import uk.vulcannetworks.api.vAPI.commands.SpawnCommand;
import uk.vulcannetworks.api.vAPI.commands.SpawnTpCommand;
import uk.vulcannetworks.bukkit.bPlugin;
import uk.vulcannetworks.chat.vChat;
import uk.vulcannetworks.inventories.MenuManager;
import uk.vulcannetworks.permissions.vPerms;
import uk.vulcannetworks.permissions.ranks.RankCommand;
import uk.vulcannetworks.sql.vSQL;
import uk.vulcannetworks.superhero.AbilityManager;
import uk.vulcannetworks.superhero.SuperHeroListener;
import uk.vulcannetworks.superhero.SuperheroManager;
import uk.vulcannetworks.superhero.commands.LevelCommand;

import java.sql.SQLException;

/**
 * Created by Vulcan on 27/11/2016.
 */
public class Install {

    private static Install install = new Install();

    private vSQL vSQL = new vSQL();
    private vAPI vAPI = new vAPI();
    private vPerms vPerms = new vPerms();
    private vChat vChat = new vChat();
    private AchievementsManager aManager = new AchievementsManager();
    private MenuManager menuManager = new MenuManager();
    private SuperheroManager superHeroManager = new SuperheroManager();
    private AbilityManager abilityManager = new AbilityManager();

    public static Install getInstance(){
        return install;
    }

    public void install(){
        installGeneralPurpose();
        installListeners();
        installCommands();
        installSchedulers();
    }

    private void installGeneralPurpose() {

        /** SuperHero **/
        abilityManager.putDefaults();

        SuperHeroListener shl = new SuperHeroListener();
        shl.runOnce();
        installListener(shl);

        /** BungeeCord **/
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(bPlugin.getInstance(), "BungeeCord");

        /** SQL **/
        try {
            getvSQL().setupConnection();
        }catch (SQLException e1) {
            e1.printStackTrace();
            Bukkit.shutdown();
        }

        /** VulcanSQL **/
        if(Bukkit.getOnlinePlayers().size() > 0){
            for(Player p : Bukkit.getOnlinePlayers()){
                vPerms.setupPlayer(p);
            }
        }

        /** VulcanAPI **/
        getvAPI().setSpawn(Bukkit.getWorld(getConfig().getString("worlds.spawn.world")), getConfig().getDouble("worlds.spawn.x"), getConfig().getDouble("worlds.spawn.y"), getConfig().getDouble("worlds.spawn.z"));

        /** Achievements **/

        getaManager().blockPlaceAchievements.add(new ReferenceBlockPlace());
        getaManager().blockBreakAchievements.add(new ReferenceBlockBreak());
        getaManager().entityAchievements.add(new HitAAdminAchievement());
        getaManager().entityAchievements.add(new HitTheDeveloperAchievement());
    }

    private FileConfiguration getConfig(){
        return bPlugin.getInstance().getConfig();
    }

    private void installListeners(){

        /** SuperHero **/
        installListener(getAbilityManager());

        /** VulcanPerms **/
        installListener(getvPerms());

        /** VulcanChat **/
        installListener(getvChat());

        /** Achievements **/
        installListener(getaManager());
        installListener(new AchievementsInventory());

        /** MenuManager **/
        installListener(getMenuManager());

    }

    private void installListener(Listener l){
        Bukkit.getPluginManager().registerEvents(l, bPlugin.getInstance());
    }

    private void installCommands(){
        Plugin p = bPlugin.getInstance();

        /** SuperHero **/
        Bukkit.getPluginCommand("exp").setExecutor(new LevelCommand());
        Bukkit.getPluginCommand("xp").setExecutor(new LevelCommand());
        Bukkit.getPluginCommand("level").setExecutor(new LevelCommand());

        /** VulcanPerms **/
        Bukkit.getPluginCommand("rank").setExecutor(new RankCommand());

        /** VulcanAPI **/
        Bukkit.getPluginCommand("setspawn").setExecutor(new SpawnCommand());
        Bukkit.getPluginCommand("set_spawn").setExecutor(new SpawnCommand());
        Bukkit.getPluginCommand("spawn").setExecutor(new SpawnTpCommand());
        Bukkit.getPluginCommand("warp").setExecutor(new SpawnTpCommand());
        Bukkit.getPluginCommand("stuck").setExecutor(new SpawnTpCommand());

        /** Achievements **/
        Bukkit.getPluginCommand("achievements").setExecutor(new AchievementsInventory());
        Bukkit.getPluginCommand("achievement").setExecutor(new AchievementsInventory());
        Bukkit.getPluginCommand("ach").setExecutor(new AchievementsInventory());

    }

    private void installSchedulers(){

        /** Configuration File Save **/
        Bukkit.getScheduler().scheduleSyncRepeatingTask(bPlugin.getInstance(), new Runnable() {
            @Override
            public void run() {
                bPlugin.getInstance().saveConfig();
            }
        }, 60, 60);

    }


    public vSQL getvSQL() {
        return vSQL;
    }

    public vAPI getvAPI() {
        return vAPI;
    }

    public vPerms getvPerms() {
        return vPerms;
    }

    public vChat getvChat() {
        return vChat;
    }

    public AchievementsManager getaManager() {
        return aManager;
    }

    public MenuManager getMenuManager() {
        return menuManager;
    }

    public SuperheroManager getSuperHeroManager() {
        return superHeroManager;
    }

    public AbilityManager getAbilityManager() {
        return abilityManager;
    }
}
