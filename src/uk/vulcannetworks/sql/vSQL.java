package uk.vulcannetworks.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import uk.vulcannetworks.bukkit.bPlugin;

public class vSQL {

	private String url, user, database, password;
	private Connection connection;
	
	public void setupConnection() throws SQLException{
		url = bPlugin.getInstance().getConfig().getString("url");
		user = bPlugin.getInstance().getConfig().getString("user");
		database = bPlugin.getInstance().getConfig().getString("database");
		password = bPlugin.getInstance().getConfig().getString("password");
		
		connection = DriverManager.getConnection("jdbc:mysql://" + url + ":3306/" + database,  user, password);
	}
	
	public Connection getRAW(){
		return connection;
	}
	
	public String readDataString(String userField, String column, String dataAssociate) throws SQLException{
		Statement statement = connection.createStatement();
		String statementSQL = "SELECT * FROM users WHERE " + userField + "='" + dataAssociate + "';";
		ResultSet resultSet = statement.executeQuery(statementSQL);
		resultSet.next();
		return resultSet.getString(column);
	}
	
	public int readDataInt(String userField, String column, String dataAssociate) throws SQLException{
		Statement statement = connection.createStatement();
		String statementSQL = "SELECT * FROM users WHERE " + userField + "='" + dataAssociate + "';";
		ResultSet resultSet = statement.executeQuery(statementSQL);
		resultSet.next();
		return resultSet.getInt(column);
	}
	
	public int readDataIntAbility(String userField, String column, String dataAssociate) throws SQLException{
		Statement statement = connection.createStatement();
		String statementSQL = "SELECT * FROM abilities WHERE " + userField + "='" + dataAssociate + "';";
		ResultSet resultSet = statement.executeQuery(statementSQL);
		resultSet.next();
		return resultSet.getInt(column);
	}
	
	public boolean readDataBoolean(String userField, String column, String dataAssociate) throws SQLException{
		Statement statement = connection.createStatement();
		String statementSQL = "SELECT * FROM users WHERE " + userField + "='" + dataAssociate + "';";
		ResultSet resultSet = statement.executeQuery(statementSQL);
		resultSet.next();
		return resultSet.getBoolean(column);
	}
	
	public void insertData(String column, String dataAssociateName, String dataAssociateRank, int level) throws SQLException{
		Statement statement = connection.createStatement();
		statement.executeUpdate("INSERT INTO users (user, rank, level) VALUES ('" + dataAssociateName + "', '" + dataAssociateRank + "', '" + level + "');");
	}
	
	public boolean contains(String userField, String dataAssociate) throws SQLException{
		Statement statement = connection.createStatement();
		String statementSQL = "SELECT * FROM users WHERE " + userField + "='" + dataAssociate + "';";
		ResultSet resultSet = statement.executeQuery(statementSQL);
		return resultSet.next();
	}
	
	public void update(String column, String userField, String dataAssociate, String data) throws SQLException{
		Statement statement = connection.createStatement();
		String statementSQL = "UPDATE users SET " + column + " = '" + data + "' WHERE " + userField + " = '" + dataAssociate + "';";
		statement.executeUpdate(statementSQL);
	}
	
	public void update(String column, String userField, String dataAssociate, int data) throws SQLException{
		Statement statement = connection.createStatement();
		String statementSQL = "UPDATE users SET " + column + " = '" + data + "' WHERE " + userField + " ='" + dataAssociate + "';";
		statement.executeUpdate(statementSQL);
	}
	
}
