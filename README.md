# VulcanAPI README #

The VulcanAPI plugin is based on Minecraft 1.10 using the Spigot API exclusively for the VulcanNetworks Minecraft Network.

### Repository Details ###

* Name: VulcanAPI
* Minecraft: 1.10
* Spigot Jar File: spigot-1.10.2-R0.1-SNAPSHOT-latest.jar
* Java JDK: JavaSE-1.8
* Version: Development Publish 1

### Contribution guidelines ###

* All code must be tested for issues and ensure functionallity with itself and the rest of the plugin.
* All code submitted immidently become copyright of VulcanNetworks and not of that of the creator unless agreed in before.
* All code must fit in with VulcanNetworks guidelines.

### Who do I talk to? ###

* VulcanNetworks Project Leads

### Vulcan Included Plugins: ###

## vPerms:

vPerms is a collection of methods inside of the VulcanAPI (vAPI) Spigot Plugin which manages permissions for the VulcanNetworks Minecraft Network.

vPerms depends on the vSQL package inside of VulcanAPI (vAPI) to function.

vPerms is a great improvement both for players, for admins and performs much better in terms of TPS with less lookups, less RAM requirements and less DISK I/O usage.

* /rank - view your rank we have stored in our MySQL database.
* /rank admin reload - reloads prefixes and permissions for players who join after the reload, this is recommended and is considered safer than a force reload, requires vulcannetworks.admin
* /rank admin set <player> <group> - update a stored rank for a ONLINE player to a specific group, requires vulcannetworks.admin
* /rank admin create group <group> - create a new rank for the local server, requires vulcannetworks.admin
* /rank admin create-add <group> <permission> - adds ONE new permission to a rank for the local server, requires vulcannetworks.admin
* /rank admin create-remove <group> <permission> - removes all permissions if possible from a rank defined from the local server, requires vulcannetworks.admin